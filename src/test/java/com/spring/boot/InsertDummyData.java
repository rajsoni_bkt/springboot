package com.spring.boot;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.springboot.Application;
import com.springboot.model.SensorData;
import com.springboot.model.SensorDetail;
import com.springboot.model.User;
import com.springboot.service.SensorDataService;
import com.springboot.service.SensorDetailService;
import com.springboot.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class InsertDummyData {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SensorDetailService sensorDetailService;
	
	@Autowired
	private SensorDataService sensorDataService;
	
	@Test
	public void insertData() {
		insertUserData();
		insertSensorDetail();
		insertSensorData();
	}

	private void insertUserData() {
		User user = new User();
		user.setId("579fc5aa0863111594gd0fb2");
		user.setName("Test");
		userService.save(user);
	}
	
	private void insertSensorDetail() {
		SensorDetail sensorDetail = new SensorDetail();
		sensorDetail.setCompanyName("Nokia");
		sensorDetail.setId("57a07301086311195d888542");
		sensorDetail.setManufacturingDate(new Date());
		sensorDetail.setUuid(UUID.randomUUID().toString());
		sensorDetailService.save(sensorDetail);
	}
	
	private void insertSensorData() {
		
		double[] data = {15.5, 1.5, 16.5};
		
		SensorData sensorData = new SensorData();
		sensorData.setMeasuredTime(new Date());
		sensorData.setSensorData(data);
		sensorData.setSensorId("57a07301086311195d888542");
		sensorData.setUserId("579fc5aa0863111594gd0fb2");
		sensorDataService.save(sensorData);
	}

}
