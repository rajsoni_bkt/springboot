package com.spring.boot.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.springboot.Application;
import com.springboot.model.SensorData;
import com.springboot.service.SensorDataService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class SensorDataServiceTest {

	@Autowired
	private SensorDataService sensorDataService;
	
	@Test
	public void findTest() {
		List<SensorData> sensorDatas = sensorDataService.findAll();
		for (SensorData sensorData : sensorDatas) {
			System.out.println("Sensor Data : " + sensorData);
		}
	}
	
	@Test
	public void findBySensorId() {
		List<SensorData> sensorDatas = sensorDataService.getBySensorId("57a07301086311195d888542");
		for (SensorData sensorData : sensorDatas) {
			System.out.println("Sensor Data by Sensor Id : " + sensorData);
		}
	}
	
	@Test
	public void findByUserId() {
		List<SensorData> sensorDatas = sensorDataService.getByUserId("579fc5aa0863111594gd0fb2");
		for (SensorData sensorData : sensorDatas) {
			System.out.println("Sensor Data By User Id : " + sensorData);
		}
	}
	
	@Test
	public void findByTime() {
		List<SensorData> sensorDatas = sensorDataService.search(1470135600000l, 1470139140000l);
		for (SensorData sensorData : sensorDatas) {
			System.out.println("Sensor Data By Time Range : " + sensorData);
		}
	}
	
}
