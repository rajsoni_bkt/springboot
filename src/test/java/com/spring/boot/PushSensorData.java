package com.spring.boot;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.springboot.Application;
import com.springboot.model.SensorData;
import com.springboot.service.SensorDataService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class PushSensorData {

    @Autowired
    private SensorDataService sensorDataService;

    @Test
    public void injectData() {

   	 while (true) {

   		 SensorData sensorData = new SensorData();
   		 sensorData.setMeasuredTime(new Date());
   		 sensorData.setUserId("579fc5aa0863111594gd0fb2"); // add user Id manually
   		 sensorData.setSensorId("57a07301086311195d888542"); // add sensor Id manually
   		 double[] randomValues = { generateRandomData(),generateRandomData(), generateRandomData() };
   		 sensorData.setSensorData(randomValues);
   		 sensorDataService.save(sensorData);

   		 try {
   			 TimeUnit.SECONDS.sleep(5);
   		 } catch (InterruptedException e) {
   			 e.printStackTrace();
   		 }
   	 }
    }

    private double generateRandomData() {
   	 Random r = new Random();
   	 double randomValue = 1.0 + (50.0 - 1.0) * r.nextDouble();
   	 DecimalFormat df = new DecimalFormat("#.##");
   	 randomValue = Double.valueOf(df.format(randomValue));
   	 return randomValue;
    }
}
