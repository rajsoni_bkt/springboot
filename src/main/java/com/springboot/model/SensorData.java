package com.springboot.model;

import java.util.Arrays;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Raj
 * 
 */
@Document(collection = "sensorData")
public class SensorData extends AbstractDocument {

	private double[] sensorData;
	private String userId;
	private Date measuredTime;
	private String sensorId;

	public double[] getSensorData() {
		return sensorData;
	}

	public void setSensorData(double[] sensorData) {
		this.sensorData = sensorData;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getMeasuredTime() {
		return measuredTime;
	}

	public void setMeasuredTime(Date measuredTime) {
		this.measuredTime = measuredTime;
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	@Override
	public String toString() {
		return "SensorData [sensorData=" + Arrays.toString(sensorData)
				+ ", userId=" + userId + ", measuredTime=" + measuredTime
				+ ", sensorId=" + sensorId + "]";
	}

}
