package com.springboot.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 
 * @author Raj
 * 
 * Contains information of sensor Device.
 *
 */
@Document(collection = "sensor_detail")
public class SensorDetail extends AbstractDocument {

	private String companyName;
	private String uuid;
	private Date manufacturingDate;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Date getManufacturingDate() {
		return manufacturingDate;
	}

	public void setManufacturingDate(Date manufacturingDate) {
		this.manufacturingDate = manufacturingDate;
	}

	@Override
	public String toString() {
		return "SensorDetail [companyName=" + companyName + ", uuid=" + uuid
				+ ", manufacturingDate=" + manufacturingDate + "]";
	}

}
