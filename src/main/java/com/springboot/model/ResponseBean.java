package com.springboot.model;

import com.springboot.constants.StatusConstants;

public class ResponseBean {

	private String message;
	private StatusConstants type;
	private int code;
	private Object data;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StatusConstants getType() {
		return type;
	}

	public void setType(StatusConstants type) {
		this.type = type;

		if (type == StatusConstants.success) {
			code = 600;
		} else {
			code = 700;
		}
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "ResponseBean [message=" + message + ", type=" + type
				+ ", code=" + code + ", data=" + data + "]";
	}

}