package com.springboot.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="user")
public class User extends AbstractDocument {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [name=" + name + "]";
	}

}
