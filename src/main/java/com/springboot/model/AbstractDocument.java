package com.springboot.model;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class AbstractDocument {

	@Id
	private String id;
	private Date createdDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "AbstractDocument [id=" + id + "]";
	}

}
