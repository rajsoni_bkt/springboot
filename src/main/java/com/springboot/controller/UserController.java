package com.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springboot.constants.MessageConstants;
import com.springboot.constants.StatusConstants;
import com.springboot.model.ResponseBean;
import com.springboot.model.User;
import com.springboot.service.UserService;
import com.springboot.util.ResponseGenerator;

@Controller
@RequestMapping("user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseBean save(@RequestBody User user) {
		if(StringUtils.isEmpty(user.getName())) {
			return ResponseGenerator.generateResponse(StatusConstants.error, MessageConstants.USER_NAME_INVALID);
		}
		return userService.insert(user);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseBean get() {
		return ResponseGenerator.generateResponse(StatusConstants.success, userService.findAll());
	}

}
