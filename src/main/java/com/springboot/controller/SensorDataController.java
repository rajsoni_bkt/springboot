package com.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springboot.constants.MessageConstants;
import com.springboot.constants.StatusConstants;
import com.springboot.model.ResponseBean;
import com.springboot.model.SensorData;
import com.springboot.service.SensorDataService;
import com.springboot.util.ResponseGenerator;

@Controller
@RequestMapping("sensorData")
public class SensorDataController {

	@Autowired
	private SensorDataService sensorDataService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseBean save(@RequestBody SensorData sensorDevice) {
		return ResponseGenerator.generateResponse(StatusConstants.success, MessageConstants.SENSOR_SAVE_SUCCESS, sensorDataService.save(sensorDevice));
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseBean get() {
		return ResponseGenerator.generateResponse(StatusConstants.success, sensorDataService.findAll());
	}

	@RequestMapping(value = "getByUserId/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseBean getByUserId(@PathVariable String userId) {
		return ResponseGenerator.generateResponse(StatusConstants.success, sensorDataService.getByUserId(userId));
	}

	@RequestMapping(value = "getBySensorId/{sensorId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseBean getBySensorId(@PathVariable String sensorId) {
		return ResponseGenerator.generateResponse(StatusConstants.success, sensorDataService.getBySensorId(sensorId));
	}
	
	@RequestMapping(value = "search", method = RequestMethod.GET)
	@ResponseBody
	public ResponseBean search(@RequestParam("from") Long from, @RequestParam("to") Long to) {
		return ResponseGenerator.generateResponse(StatusConstants.success, sensorDataService.search(from, to));
	}
	
}