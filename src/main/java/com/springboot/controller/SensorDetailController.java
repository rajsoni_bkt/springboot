package com.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springboot.constants.MessageConstants;
import com.springboot.constants.StatusConstants;
import com.springboot.model.ResponseBean;
import com.springboot.model.SensorDetail;
import com.springboot.service.SensorDetailService;
import com.springboot.util.ResponseGenerator;

@Controller
@RequestMapping("sensorDetail")
public class SensorDetailController {

	@Autowired
	private SensorDetailService sensorDetailService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseBean save(@RequestBody SensorDetail sensorDetail) {
		return ResponseGenerator.generateResponse(StatusConstants.success, MessageConstants.SENSOR_DETAIL_SAVE_SUCCESS, sensorDetailService.save(sensorDetail));
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseBean get() {
		return ResponseGenerator.generateResponse(StatusConstants.success, sensorDetailService.findAll());
	}

}
