package com.springboot.dao;

import com.springboot.model.User;

public interface UserDao extends AbstractDao<User, String> {

	public int countByNameEquals(String name);
	
}
