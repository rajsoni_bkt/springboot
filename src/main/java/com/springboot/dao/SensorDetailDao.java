package com.springboot.dao;

import com.springboot.model.SensorDetail;

public interface SensorDetailDao extends AbstractDao<SensorDetail, String> {

}
