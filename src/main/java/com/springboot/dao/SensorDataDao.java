package com.springboot.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.Query;

import com.springboot.model.SensorData;

public interface SensorDataDao extends AbstractDao<SensorData, String> {

	public List<SensorData> findByUserId(String userId);

	public List<SensorData> findBySensorId(String sensorId);

	@Query("{'measuredTime' : {$gt : ?0, $lt : ?1}}")
	public List<SensorData> searchByDate(Date from, Date to);

}
