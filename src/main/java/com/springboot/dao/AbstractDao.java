package com.springboot.dao;

import java.io.Serializable;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.springboot.model.AbstractDocument;

public interface AbstractDao <T extends AbstractDocument, ID extends Serializable> extends MongoRepository<T, Serializable> {
	
}
