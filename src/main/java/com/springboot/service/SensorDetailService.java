package com.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.dao.SensorDetailDao;
import com.springboot.model.SensorDetail;

@Service
public class SensorDetailService extends AbstractService<SensorDetail, String> {

	private SensorDetailDao sensorDetailDao;

	@Autowired
	public SensorDetailService(SensorDetailDao sensorDetailDao) {
		super(sensorDetailDao);
		this.sensorDetailDao = sensorDetailDao;
	}
	
}

