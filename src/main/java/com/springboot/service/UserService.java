package com.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.constants.MessageConstants;
import com.springboot.constants.StatusConstants;
import com.springboot.dao.UserDao;
import com.springboot.model.ResponseBean;
import com.springboot.model.User;
import com.springboot.util.ResponseGenerator;

@Service
public class UserService extends AbstractService<User, String> {

	private UserDao userDao;
	
	@Autowired
	public UserService(UserDao userDao) {
		super(userDao);
		this.userDao = userDao;
	}
	
	public ResponseBean insert(User user) {
		if(userDao.countByNameEquals(user.getName()) > 0) {
			return ResponseGenerator.generateResponse(StatusConstants.error, MessageConstants.USER_NAME_EXISTS);
		}
		return ResponseGenerator.generateResponse(StatusConstants.success, MessageConstants.USER_SAVE_SUCCESS, super.save(user));
	}
	
}
