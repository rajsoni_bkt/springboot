package com.springboot.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.dao.SensorDataDao;
import com.springboot.model.SensorData;

@Service
public class SensorDataService extends AbstractService<SensorData, String> {

	private final SensorDataDao sensorDataDao;

	@Autowired
	public SensorDataService(SensorDataDao sensorDataDao) {
		super(sensorDataDao);
		this.sensorDataDao = sensorDataDao;
	}

	/**
	 * Finds Sensor Data based on user Id
	 * 
	 * @param userId
	 * @return
	 */
	public List<SensorData> getByUserId(String userId) {
		return sensorDataDao.findByUserId(userId);
	}
	
	/**
	 * Finds Sensor Data based on sensor Id
	 * 
	 * @param sensorId
	 * @return
	 */
	public List<SensorData> getBySensorId(String sensorId) {
		return sensorDataDao.findBySensorId(sensorId);
	}

	/**
	 * Finds Sensor Data based on time range
	 * 
	 * @param from -> start datetime
	 * @param to -> end datetime
	 * @return List<SensorData>
	 */
	public List<SensorData> search(Long from, Long to) {
		return sensorDataDao.searchByDate(new Date(from), new Date(to));
	}

}
