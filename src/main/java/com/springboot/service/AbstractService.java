package com.springboot.service;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.springboot.dao.AbstractDao;
import com.springboot.model.AbstractDocument;

public class AbstractService <T extends AbstractDocument, ID extends Serializable>  {

	protected AbstractDao<T, String> abstractDao;
	
	public AbstractService(AbstractDao<T, String> abstractDao) {
		this.abstractDao = abstractDao;
	}
	
	public T save(T T) {
		T.setCreatedDate(new Date());
		return abstractDao.save(T);
	}
	
	public List<T> findAll() {
		return abstractDao.findAll();
	}
	
	public T findById(String id) {
		return abstractDao.findOne(id);
	}
	
	public void delete(T t) {
		abstractDao.delete(t.getId());
	}
	
}
