package com.springboot.util;

import com.springboot.constants.StatusConstants;
import com.springboot.model.ResponseBean;

public class ResponseGenerator {

	public static ResponseBean generateResponse(StatusConstants status, String message , Object data) {
		ResponseBean resObj = new ResponseBean();
		resObj.setType(status);
		resObj.setMessage(message);
		resObj.setData(data);
		return resObj;
	}
	
	public static ResponseBean generateResponse(StatusConstants status, String message) {
		ResponseBean resObj = new ResponseBean();
		resObj.setType(status);
		resObj.setMessage(message);
		return resObj;
	}
	
	public static ResponseBean generateResponse(StatusConstants status, Object data) {
		ResponseBean resObj = new ResponseBean();
		resObj.setType(status);
		resObj.setData(data);
		return resObj;
	}
}