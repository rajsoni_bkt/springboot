package com.springboot.constants;

public class MessageConstants {
	
	/************************ User Messages ***********************/
	public static final String USER_SAVE_SUCCESS =  "User saved Successfully.";
	public static final String USER_NAME_INVALID =  "Invalid User name!";
	public static final String USER_NAME_EXISTS =  "User name already exists!";
	
	/************************ Sensor Data Messages ***********************/
	public static final String SENSOR_SAVE_SUCCESS =  "Sensor data saved Successfully.";
	
	/************************ Sensor Data Messages ***********************/
	public static final String SENSOR_DETAIL_SAVE_SUCCESS =  "Sensor detail saved Successfully.";
	
	
}
